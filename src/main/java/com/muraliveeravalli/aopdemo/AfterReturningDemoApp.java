package com.muraliveeravalli.aopdemo;

import com.muraliveeravalli.aopdemo.dao.AccountDAO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AfterReturningDemoApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
        AccountDAO accountDAO = context.getBean(AccountDAO.class);
        try {
            System.out.println(accountDAO.findAccounts());
        } catch (Exception e) {
            e.printStackTrace();
        }
        context.close();
    }
}
