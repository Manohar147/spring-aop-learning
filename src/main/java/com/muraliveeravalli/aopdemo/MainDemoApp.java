package com.muraliveeravalli.aopdemo;

import com.muraliveeravalli.aopdemo.dao.AccountDAO;
import com.muraliveeravalli.aopdemo.dao.MembershipDAO;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainDemoApp {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
        AccountDAO accountDAO = context.getBean(AccountDAO.class);
        MembershipDAO membershipDAO = context.getBean(MembershipDAO.class);
        Account account = new Account("Personal", "level 2");
        accountDAO.addAccount(account, true);
        accountDAO.doWork();
        accountDAO.getName();
        accountDAO.getServiceCode();
        accountDAO.setName("Silva");
        accountDAO.setServiceCode("12341123");
        membershipDAO.addAccount();
        membershipDAO.addSillyMember();
        membershipDAO.goToSleep();
        accountDAO.findAccounts();

        context.close();
    }
}
