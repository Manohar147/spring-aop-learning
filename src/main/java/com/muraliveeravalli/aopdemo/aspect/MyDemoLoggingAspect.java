//package com.muraliveeravalli.aopdemo.aspect;
//
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class MyDemoLoggingAspect {
//
////    @Before("execution(public void  addAccount())")
////    public void beforeAddAccountAdvice() {
////        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
////    }
//
////    @Before("execution(public void  com.muraliveeravalli.aopdemo.dao.AccountDAO.addAccount())")
////    public void beforeAddAccountAdvice() {
////        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
////    }
//
//    //    @Before("execution(public void  add*())")
////    public void beforeAddAccountAdvice() {
////        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
////    }
//    //    @Before("execution(public void  addAccount())") Match all methods with name addAccount() with return type void and modifier public
//    //    @Before("execution(public void  com.muraliveeravalli.aopdemo.dao.AccountDAO.addAccount())") match only in specific class
//    //    @Before("execution(public void  add*())") match any methods which starts with add with return type void and modifier public
////    @Before("execution(void  add*())")  Modifier is optional so giving wild card is not relevant
////    @Before("execution(* add*())") // Match any return type and any method which starts with add and any modifier
////    @Before("execution(* add*(com.muraliveeravalli.aopdemo.Account))") // Match any return type and any method which starts with add and any modifier but with one parameter 'Account'
////    @Before("execution(* add*(..))")
//// Match any return type and any method which starts with add and any modifier with any number of parameters
////    @Before("execution(* add*(com.muraliveeravalli.aopdemo.Account,..))")
////// Match any return type and any method which starts with add and any modifier but with one parameter 'Account' and any other parameter
////    @Before("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
//// Match any return type and any method in dao package with any number of parameters
//    public void beforeAddAccountAdvice() {
//        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
//    }
//}
