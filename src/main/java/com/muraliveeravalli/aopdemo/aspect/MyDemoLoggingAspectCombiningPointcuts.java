//package com.muraliveeravalli.aopdemo.aspect;
//
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class MyDemoLoggingAspectCombiningPointcuts {
//
////    //Point cut declaration
////    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
////    private static void forDaoPackage() {
////    }
////
////    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.get*(..))")
////    private static void gettersForDao() {
////    }
////
////    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.set*(..))")
////    private static void settersForDao() {
////    }
////
////    @Pointcut("forDaoPackage() && !(gettersForDao() || settersForDao())")
////    private void forDaoNoGetterAndSetter() {
////
////    }
////
////
////    @Before("forDaoNoGetterAndSetter()")
////    public void beforeAddAccountAdvice() {
////        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
////    }
////
////    @Before("forDaoNoGetterAndSetter()")
////    public void performApiAnalytics() {
////        System.out.println("\n======>>>> Doing some analytics");
////    }
//}
