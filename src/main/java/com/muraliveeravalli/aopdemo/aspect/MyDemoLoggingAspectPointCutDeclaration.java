//package com.muraliveeravalli.aopdemo.aspect;
//
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//public class MyDemoLoggingAspectPointCutDeclaration {
////    //Point cut declaration
////    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
////    private static void forDaoPackage() {
////    }
////
////    @Before("forDaoPackage()")
////    public void beforeAddAccountAdvice() {
////        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
////    }
////
////    @Before("forDaoPackage()")
////    public void performApiAnalytics() {
////        System.out.println("\n======>>>> Doing some analytics");
////    }
//}
