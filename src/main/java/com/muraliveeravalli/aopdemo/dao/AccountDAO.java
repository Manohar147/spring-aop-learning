package com.muraliveeravalli.aopdemo.dao;

import com.muraliveeravalli.aopdemo.Account;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AccountDAO {

    @Value("Murali")
    private String name;

    @Value("11223344")
    private String serviceCode;

    public void addAccount(Account account, boolean vipFlag) {
        System.out.println(getClass() + ": Doing my DB work: Add Account " + account + ", vipFlag: " + vipFlag);
    }

    public boolean doWork() {
        System.out.println(getClass() + ": Doing work...");
        return false;
    }

    public String getName() {
        System.out.println("in getName()");
        return name;
    }

    public void setName(String name) {
        System.out.println("in setName()");
        this.name = name;
    }

    public String getServiceCode() {
        System.out.println("in getServiceCode()");
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        System.out.println("in setServiceCode()");
        this.serviceCode = serviceCode;
    }

    public List<Account> findAccounts() {

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        List<Account> list = new ArrayList<>();
////        try {
//        list.add(new Account("Murali", "1"));
//        list.add(new Account("Manohar", "1"));
//        list.add(new Account("Pavan", "1"));
//        } catch (Exception e) {
//
//        }
        throw new RuntimeException("No soup for you!!");
//        return list;
    }
}
