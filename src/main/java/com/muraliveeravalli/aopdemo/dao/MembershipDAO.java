package com.muraliveeravalli.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {
    public void addAccount() {
        System.out.println(getClass() + ": Doing some stuff in MembershipDAO");
    }

    public boolean addSillyMember() {
        System.out.println(getClass() + ": In Add silly member");
        return false;
    }

    public void goToSleep() {
        System.out.println(getClass() + ": going to sleep... -.-");
    }

}
