package com.muraliveeravalli.aopdemo.orderAspects;

import com.muraliveeravalli.aopdemo.Account;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Aspect
@Component
@Order(2)
public class MyApiAnalyticsAspect {
    //Point cut declaration
    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
    private static void forDaoPackage() {
    }

    @AfterReturning(
            pointcut = "com.muraliveeravalli.aopdemo.orderAspects.MyApiAnalyticsAspect.forDaoPackage()",
            returning = "value"
    )

    //Modifying Data
    private void afterReturningFindAccounts(JoinPoint joinPoint, List<Account> value) {
        String method = joinPoint.getSignature().toShortString();
        System.out.println("Method: " + method + "Value: " + value);
        value = value.stream().map(thiz -> {
            thiz.setName(thiz.getName().toUpperCase());
            return thiz;
        }).collect(Collectors.toList());

        System.out.println("---" + value);

    }

    @Before("forDaoPackage()")
    public void performApiAnalytics() {
        System.out.println("\n======>>>> Doing some analytics");
    }

}
