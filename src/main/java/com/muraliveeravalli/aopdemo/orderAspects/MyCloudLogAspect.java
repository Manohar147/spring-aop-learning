package com.muraliveeravalli.aopdemo.orderAspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(1)
public class MyCloudLogAspect {
    //Point cut declaration
    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
    private static void forDaoPackage() {
    }

    @Before("forDaoPackage()")
    public void logToCloundAspect() {
        System.out.println("\n======>>>> doing some cloud stuff...");
    }
}
