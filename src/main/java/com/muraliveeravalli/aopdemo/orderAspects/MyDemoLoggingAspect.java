package com.muraliveeravalli.aopdemo.orderAspects;

import com.muraliveeravalli.aopdemo.Account;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Order(3)
public class MyDemoLoggingAspect {
    //Point cut declaration
    @Pointcut("execution(* com.muraliveeravalli.aopdemo.dao.*.*(..))")
    private static void forDaoPackage() {
    }

    @Pointcut("execution(java.util.List<com.muraliveeravalli.aopdemo.Account> com.muraliveeravalli.aopdemo.dao.*.*(..))")
    public static void findAccounts() {
    }

    @AfterReturning(
            pointcut = "findAccounts()",
            returning = "value"
    )
    private void afterReturningFindAccounts(JoinPoint joinPoint, Object value) {
        String method = joinPoint.getSignature().toShortString();
        System.out.println("In @AfterReturning");
        System.out.println("Method: " + method + "Value: " + value);
    }

    @AfterThrowing(
            pointcut = "findAccounts()",
            throwing = "e"
    )
    private void afterThrowingFindAccounts(JoinPoint joinPoint, Throwable e) {
        String method = joinPoint.getSignature().toShortString();
        System.out.println("In @AfterThrowing");
        System.out.println("Method: " + method + "Exception: " + e.getMessage());
    }

    @After("findAccounts()")
    private void afterFindAccounts(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().toShortString();
        System.out.println("In @After");
        System.out.println("Method: " + method);
    }

    @Around("findAccounts()")
    private Object aroundFindAccounts(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("In  @Around");
        long start = System.currentTimeMillis();
        Object o = null;
        //Handling the exception
        try {
            o = proceedingJoinPoint.proceed();

        } catch (Exception e) {
            throw e;
        }
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Duration : " + duration);
        return o;
    }


    @Before("forDaoPackage()")
    public void beforeAddAccountAdvice(JoinPoint joinPoint) {
        System.out.println("\n======>>>> Executing @Before advice on addAccount()");
        //display method signature
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        System.out.println("MethodSignature: " + methodSignature);
        Object args[] = joinPoint.getArgs();
        System.out.println(Arrays.toString(args));
        for (Object arg : args) {
            System.out.println("arg: " + arg);
            if (arg instanceof Account) {
                Account account = (Account) arg;
                System.out.println("Account Name: " + account.getName());
            }
        }

    }
}
